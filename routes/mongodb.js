var MongoClient = require('mongodb').MongoClient;
var Promise = require('promise');


// this function will create a mongoDB connection at IP=replicaset ip, PORT= 27017
function connection() {
    var url = process.env.CONNECTION_URL;
    return new Promise(function (resolve, reject) {
        MongoClient.connect(url, function (err, db) {
            if (err) {
                console.log(err);
                reject(err);
            } else {
                console.log("connection to mongo is successuful");
                resolve(db);
            }
        });
    });

}

//this function will return all the users.
function find(conn,search) {
    let dbName= process.env.DB_NAME;
    let collectionName = process.env.COLLECTION_NAME;
    let dbo = conn.db(dbName);
    return new Promise(function (resolve, reject) {
        dbo.collection(collectionName).find(search)
            .toArray(function (err, result) {
                if (err) {
                    reject(err);
                } else {
                    console.log(result);
                    resolve(result);
                }
                conn.close();
            });
    });
}



//export your functions to outside
module.exports = {
    connection: connection,
    find: find
}