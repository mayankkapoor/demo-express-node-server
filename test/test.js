const request = require('supertest');
const app = require('../app');
var addBl = require('../bl/add');
var chai = require('chai')
chai.use(require('sinon-chai'));
var expect = chai.expect;
var sinon = require('sinon');


describe('App', function() {
  it('has the default page', function(done) {
    request(app)
      .get('/')
      .expect(/Welcome to Express/, done);
  });

  it('Test addNumbers functionality and BL', function(done) {
    let a = 3;
    let b = 4;
    let sum = addBl.addNumbers(a,b);
    let expectedSum = 7;
    expect(sum).to.be.equal(expectedSum);
    done();
  })
    
}); 
